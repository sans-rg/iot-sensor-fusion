# -*- coding: utf-8 -*-
"""

@author: joseb
        Data must be placed in "./Data_Fusion_paper/" path.
        Results are written in "../Results2/KNN_PLS/" path.


"""

import matplotlib.pyplot as plt
import pandas as pd
from sklearn import linear_model
from sklearn.metrics import mean_squared_error,r2_score
import numpy as np
import os
from Calib_CAPTOR_Classes import DataSet
import argparse
from argparse import RawTextHelpFormatter
from sklearn.cross_decomposition import PLSRegression

parser = argparse.ArgumentParser(description='Fusion of Ozone Sensors',formatter_class=RawTextHelpFormatter)
parser.add_argument("-f", "--captor_file", default=None, help = 'file with the CSV data')
parser.add_argument("-n", "--captor_name", default=None, help = 'Name of the Captor')
parser.add_argument("-p", "--Place_ER", default=None, help = 'Name of the Reference Station')
parser.add_argument("-s", "--sensors_c", default=1, help = 'Sensors to calibrate, e.g. 134, 1=s1, 3=s3, and 4=s4')


args = parser.parse_args()
file_Captor = args.captor_file
CAPTOR_NUMBER = args.captor_name
# Train/Test split percentage
TRAIN_SIZE = 0.75
TEST_SIZE = 1.0 - TRAIN_SIZE
DATE_FORM_NEW = "%Y-%m-%d %H:%M"
dir_path = os.getcwd() + "/Data_Fusion_paper/"
data_file = dir_path + file_Captor
DataS = DataSet(TRAIN_SIZE)
# Constant variable, can be changed to admit more sensors
sensors = ['s1_ec', 's1', 's2', 's3', 's4']
s = args.sensors_c
sens_to_cal = []
for sen in s:
        sens_to_cal.append(sensors[int(sen)])
dir_wr = os.getcwd() + "/../Results2/KNN_PLS/"

def ScatterPlot(data1,data2,sensor1,sensor2,color):
    plt.scatter(data1,data2,color=color,label=sensor1)
    plt.xlabel(sensor1,fontsize=13)
    plt.ylabel(sensor2,fontsize=13)
    plt.legend(loc='upper left')
    plt.show()

def Plot_Concentrations(data1,data2,sensor1,sensor2,ylabel,color):
    x = np.arange(0, len(data1), 1)
    plt.plot(x,data2,color=color,linewidth=1.75, linestyle="-",label=sensor1)
    plt.plot(x,data1,color='black',linewidth=1.75, linestyle="-",label=sensor2)
    plt.xlabel('TICS 1h',fontsize=13)
    plt.ylabel(ylabel,fontsize=13)
#    plt.legend(loc='upper right')
    plt.show()

def KNN_data(data,EstRef,sensor,conc):
    data_train = data.sample(frac=TRAIN_SIZE,random_state=200)
    data_test  = data.drop(data_train.index)

    fit = []
    for i in sensor:
        fit.append(i)
    fit.append('Temp')
    fit.append('RH')
    X_train= data_train[fit]
    X_test= data_test[fit]
    X_all = data[fit]
    Y_train = data_train[EstRef]
    Y_test = data_test[EstRef]
    Y_all = data[EstRef]
    pls = PLSRegression(n_components = X_train.shape[1])
    pls = pls.fit(X_train, Y_train)
    X_train = pls.transform(X_train)
    model, params = DataS.train_tune_KNN(X_train, Y_train)
    params_sensors = []
    params_sensors.append(CAPTOR_NUMBER)
    params_sensors.append(sensor[0])
    params_sensors.append(params)

    model = model.fit(X_train,Y_train)

    data_predict_train=model.predict(X_train)
    RMSE_train = np.sqrt(mean_squared_error(Y_train, data_predict_train))
    R2_train = r2_score(Y_train, data_predict_train)

    X_test = pls.transform(X_test)
    data_predict_test = model.predict(X_test)
    RMSE_test = np.sqrt(mean_squared_error(Y_test, data_predict_test))
    R2_test = r2_score(Y_test, data_predict_test)

    X_all = pls.transform(X_all)
    data_predict_all = model.predict(X_all)
    RMSE_all = np.sqrt(mean_squared_error(Y_all, data_predict_all))
    R2_all = r2_score(Y_all, data_predict_all)

#    ScatterPlot(data_predict_train,Y_train,sensor[0],EstRef,'red')
#    ScatterPlot(data_predict_test,Y_test,sensor[0],EstRef,'blue')
    # ScatterPlot(data_predict_all,Y_all,sensor[0],EstRef,'green')

#    Plot_Concentrations(data_predict_train,Y_train,sensor[0],EstRef,conc+' RMSE ($\mu$gr/m$^3$)','red')
#    Plot_Concentrations(data_predict_test,Y_test,sensor[0],EstRef,conc+' RMSE ($\mu$gr/m$^3$)','blue')
    # Plot_Concentrations(data_predict_all,Y_all,sensor[0],EstRef,conc+' RMSE ($\mu$gr/m$^3$)','green')

    return data_predict_test,params_sensors,[RMSE_train,RMSE_test,RMSE_all],[R2_train,R2_test,R2_all]
#%%
data = pd.read_csv(data_file,delimiter=';')
print data.count()
data_predict_all,betas,RMSE,R2 = KNN_data(data,'ref',sens_to_cal,'O3')
print 'RMSE TRAIN: %.3f R2 TRAIN: %.3f' %(RMSE[0],R2[0])
print 'RMSE TEST: %.3f R2 TEST: %.3f' %(RMSE[1],R2[1])
print 'RMSE ALL: %.3f R2 ALL: %.3f' %(RMSE[2],R2[2])

results_df = pd.DataFrame()
results_df['predicted'] = data_predict_all
results_2 = pd.DataFrame({'params': betas})
results_3 = pd.DataFrame({'RMSE': RMSE})
results_4 = pd.DataFrame({'R2': R2})
results_df = pd.concat([results_df, results_2, results_3, results_4], ignore_index = True, axis = 1)
name = CAPTOR_NUMBER
for s in sens_to_cal:
        name = name + "_" + s
name = name + ".csv"

results_df.to_csv(dir_wr + name, sep = ";")
