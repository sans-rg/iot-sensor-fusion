# Sensor Fusion Calibration #

This repository contains python scripts to calibrate tropospheric ozone sensor data obtained in the H2020 Captor project using sensor fusion techniques. Four different models are implemented; Multiple Linear Regression (MLR), K-Nearest Neighbors (KNN),Random Forest(RF) and Support Vector Regression (SVR). The methodology consists of first applying the PLS procedure to derive orthogonal components (to avoid multicollinearity problems). Afterwards, the components are used as features in the machine learning algorithms, so the models are trained. The scripts available in this repository have been used in the elaboration of the paper: "Multi-sensor data fusion calibration in IoT air pollution platforms" submitted to the IEEE Internet of Things journal.
### Script execution examples: Machine Learing###

The different methods can be executed by executing the python scripts {Method}_Calib_Pandas_CMD_PLS.py. If you want to add more sensors to calibrate you should change the "sensors" constant and add the sensor names as they appear in the CSV File. Moreover, the temperature and relative humidity fields must be named "Temp" and "RH" respectively in the CSV file.

Execution:

$ python {Method}_Calib_Pandas_CMD_PLS.py -f data_file -n name_for_results -p place -s array_sensors_to_calibrate

Here is an execution example:

$ python MLR_Calib_Pandas_CMD_PLS.py -f fusion_manlleu_supervised.csv -n manlleu_2 -p MANLLEU -s 01

The input file is: fusion_manlleu_supervised.csv. The first two sensors present in the csv file ( -s 01) are calibrated.  

### Script execution examples: Weighted Averages###

File "Calib_CAPTOR_Classes.py" contains the function "sensor_combination_weighted_averages(errors, y_hat_test)", in which provided the errors of the different sensors in the training set and the sensors predictions or the test set, returns the set of computed weights and the test predictions for the fusion.

### Data ###

The data folder is includes the five different data sets used in the paper along with a metadata file, where the different features are explained.
