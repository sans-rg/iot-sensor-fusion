# -*- coding: utf-8 -*-
"""
        Sensor fusion:
                fusion by weighted averages with correlated errors
"""


import numpy as np
import csv
import numpy as np
import matplotlib.pyplot as plt
import matplotlib as mpl
import seaborn as sns
import math
from matplotlib.ticker import MaxNLocator
from sklearn.metrics import mean_squared_error,r2_score
import os
import pandas as pd



def sensor_fusion_combination(y_hat_train, y_hat_test, y_test):
        covariance = y_hat_train.cov()
        inv_cov = np.linalg.inv(covariance.values)
        weights = [0.] * y_hat_train.shape[1]
        for i in range(y_hat_train.shape[1]):
                weights[i] = sum(inv_cov[i,:])/(inv_cov.sum())
        y_fusion = []
        for i in range(y_hat_test.shape[0]):
                aux = 0.0
                for j in range(y_hat_test.shape[1]):
                        aux += weights[j]*y_hat_test.iloc[i][j]
                y_fusion.append(aux)
        rmse = np.sqrt(mean_squared_error(np.array(y_test), np.array(y_fusion)))
        return rmse



#%%

folder = os.getcwd() + "/Results2/MLR_PLS/"
file_rd = "RAP-CAP-2017_"
combinations = {}
combinations['1'] = ['s1']
combinations['2'] = [['s1', 's2'], ['s1','s3'], ['s1','s4']]
combinations['3'] = [['s1', 's2', 's3'], ['s1', 's2', 's4'], ['s1', 's3', 's4']]
combinations['4'] = [['s1', 's2', 's3', 's4']]
combinations_keys = ['1', '2', '3', '4']


avg_RMSE_val = {}
avg_RMSE_val_std = {}
for method in ["MLR", "KNN", "RF", "SVR"]:
        avg_RMSE_val[method] = []
        avg_RMSE_val_std[method] = []
        folder = os.getcwd() + "/Results2/" + method + "_PLS/"
        for key in combinations_keys:
                aux_avg_val_RMSE = []
                if key != '1':
                        for comb in combinations[key]:
                                y_predicted_train = pd.DataFrame()
                                y_predicted_test  = pd.DataFrame()
                                y_test = []
                                for prefix in comb:
                                        file_to_rd = file_rd + prefix
                                        data = pd.read_csv(folder + file_to_rd + ".csv", sep = ";")
                                        y_predicted_train[prefix] = data['5'].dropna() - data['6'].dropna()
                                        y_predicted_test[prefix] = data['0'].dropna()
                                        y_test = data['1'].dropna()
                                        # Apply sensor fusion algorithm

                                rmse_test = sensor_fusion_combination(y_predicted_train, y_predicted_test, y_test)
                                aux_avg_val_RMSE.append(rmse_test)
                        avg_RMSE_val[method].append(np.mean(aux_avg_val_RMSE))
                else:
                        file_to_rd = file_rd + combinations[key][0]
                        data = pd.read_csv(folder + file_to_rd + ".csv", sep = ";")
                        print(data)
                        avg_RMSE_val[method].append(data['3'][1])

                if key == '1' or key == '4':
                        avg_RMSE_val_std[method].append(0)
                else:
                        avg_RMSE_val_std[method].append(np.std(aux_avg_val_RMSE, ddof = 1))

#%%
file_rd = "RAP-CAP-2017_"
combinations = {}
combinations['1'] = [['s1', 's1_ec']]
combinations['2'] = [['s1', 's2', 's1_ec'], ['s1','s3', 's1_ec'], ['s1','s4', 's1_ec']]
combinations['3'] = [['s1', 's2', 's3', 's1_ec'], ['s1', 's2', 's4', 's1_ec'], ['s1', 's3', 's4', 's1_ec']]
combinations['4'] = [['s1', 's2', 's3', 's4', 's1_ec']]
combinations_keys = ['1', '2', '3', '4']
#combinations['1'] = ['s2']
#combinations['2'] = ['s2_s1', 's2_s3', 's2_s4']
#combinations['3'] = ['s2_s1_s3', 's2_s1_s4', 's2_s3_s4']
#combinations['4'] = ['s1_s2_s3_s4']

avg_RMSE_val_ec = {}
avg_RMSE_val_ec_std = {}
for method in ["MLR", "KNN", "RF", "SVR"]:
        avg_RMSE_val_ec[method] = []
        avg_RMSE_val_ec_std[method] = []
        folder = os.getcwd() + "/Results2/" + method + "_PLS/"
        for key in combinations_keys:
                aux_avg_val_RMSE = []
                if key != '6':
                        for comb in combinations[key]:
                                y_predicted_train = pd.DataFrame()
                                y_predicted_test  = pd.DataFrame()
                                y_test = []
                                for prefix in comb:
                                        file_to_rd = file_rd  + prefix
                                        data = pd.read_csv(folder + file_to_rd + ".csv", sep = ";")
                                        y_predicted_train[prefix] = data['5'].dropna() - (data['6'].dropna())
                                        y_predicted_test[prefix] = data['0'].dropna()
                                        y_test = data['1'].dropna()
                                        # Apply sensor fusion algorithm

                                rmse_test = sensor_fusion_combination(y_predicted_train, y_predicted_test, y_test)
                                aux_avg_val_RMSE.append(rmse_test)
                        avg_RMSE_val_ec[method].append(np.mean(aux_avg_val_RMSE))
                else:
                        file_to_rd = file_rd + combinations[key][0]
                        data = pd.read_csv(folder  + file_to_rd + ".csv", sep = ";")
                        avg_RMSE_val_ec[method].append(data['3'][1])

                if key == '1' or key == '4':
                        avg_RMSE_val_ec_std[method].append(0)
                else:
                        avg_RMSE_val_ec_std[method].append(np.std(aux_avg_val_RMSE, ddof = 1))


mpl.rcParams['text.latex.preamble']=[r"\usepackage{amsmath}"]
mpl.rcParams['figure.figsize'] = 6, 5.5
color = ['black','red','blue','magenta','green','orange']
col_methods = {}
col_methods["mlr"] = color[3]
col_methods["knn"] = color[1]
col_methods["rf"] = color[2]
col_methods["svr"] = color[4]
plt.rc('font', family='serif')



mpl.rcParams['figure.figsize'] = 8, 5
fig, axs = plt.subplots(1, 2, sharey=True)
# Remove horizontal space between axes
#yticks = ticker.MaxNLocator(nbins = 10)
#axs[0].xaxis.set_major_locator(yticks)
#axs[1].xaxis.set_major_locator(yticks)
x = np.arange(start=1, stop = 5, step = 1)

fig.subplots_adjust(hspace=0)
t_025_2 = 4.303
plt.setp(axs[0].get_xticklabels(), visible=True)
axs[0].plot(x, avg_RMSE_val["MLR"],  c = col_methods["mlr"], marker = "o")
axs[0].fill_between(x, np.array(avg_RMSE_val["MLR"]) + t_025_2*np.array(avg_RMSE_val_std["MLR"])/math.sqrt(3), np.array(avg_RMSE_val["MLR"]) - t_025_2*np.array(avg_RMSE_val_std["MLR"])/math.sqrt(3), facecolor=col_methods["mlr"], alpha=0.3)
axs[0].plot(x, avg_RMSE_val["KNN"],  c = col_methods["knn"], marker = "o")
axs[0].fill_between(x, np.array(avg_RMSE_val["KNN"]) + t_025_2*np.array(avg_RMSE_val_std["KNN"])/math.sqrt(3), np.array(avg_RMSE_val["KNN"]) - t_025_2*np.array(avg_RMSE_val_std["KNN"])/math.sqrt(3), facecolor=col_methods["knn"], alpha=0.3)

axs[0].plot(x, avg_RMSE_val["RF"], c = col_methods["rf"], marker = "o")
axs[0].fill_between(x, np.array(avg_RMSE_val["RF"]) + t_025_2*np.array(avg_RMSE_val_std["RF"])/math.sqrt(3), np.array(avg_RMSE_val["RF"]) - t_025_2*np.array(avg_RMSE_val_std["RF"])/math.sqrt(3), facecolor=col_methods["rf"], alpha=0.3)

axs[0].plot(x, avg_RMSE_val["SVR"], c = col_methods["svr"], marker = "o")
axs[0].fill_between(x, np.array(avg_RMSE_val["SVR"]) + t_025_2*np.array(avg_RMSE_val_std["SVR"])/math.sqrt(3), np.array(avg_RMSE_val["SVR"]) - t_025_2*np.array(avg_RMSE_val_std["SVR"])/math.sqrt(3), facecolor=col_methods["svr"], alpha=0.3)

#axs[0].axhline(y = 22.65, label = "MLR EC", ls = "--", c = col_methods["mlr"])
#axs[0].axhline(y = 18.39, label = "KNN EC", ls = "--", c = col_methods["knn"])
#axs[0].axhline(y = 19.19, label = "RF EC", ls = "--", c = col_methods["rf"])
#axs[0].axhline(y = 18.93, label = "SVR EC", ls = "--", c = col_methods["svr"])

# 2017
axs[0].axhline(y = 9.29, label = "MLR EC", ls = "--", c = col_methods["mlr"])
axs[0].axhline(y = 8.39, label = "KNN EC", ls = "--", c = col_methods["knn"])
axs[0].axhline(y = 8.4, label = "RF EC", ls = "--", c = col_methods["rf"])
axs[0].axhline(y = 7.97, label = "SVR EC", ls = "--", c = col_methods["svr"])

## 2019 FUSION
#axs[0].axhline(y = 21.02, label = "MLR EC", ls = "--", c = col_methods["mlr"])
#axs[0].axhline(y = 19.27, label = "KNN EC", ls = "--", c = col_methods["knn"])
#axs[0].axhline(y = 19.58, label = "RF EC", ls = "--", c = col_methods["rf"])
#axs[0].axhline(y = 19.65, label = "SVR EC", ls = "--", c = col_methods["svr"])
#

axs[1].plot(x, avg_RMSE_val_ec["MLR"], c = col_methods["mlr"],label = "MLR", linestyle = "--",marker = "o")
axs[1].fill_between(x, np.array(avg_RMSE_val_ec["MLR"]) + t_025_2*np.array(avg_RMSE_val_ec_std["MLR"])/math.sqrt(3), np.array(avg_RMSE_val_ec["MLR"]) - t_025_2*np.array(avg_RMSE_val_ec_std["MLR"])/math.sqrt(3), facecolor=col_methods["mlr"], alpha=0.3)
axs[1].plot(x, avg_RMSE_val_ec["KNN"],  c = col_methods["knn"],label = "KNN", linestyle = "--", marker = "o")
axs[1].fill_between(x, np.array(avg_RMSE_val_ec["KNN"]) + t_025_2*np.array(avg_RMSE_val_ec_std["KNN"])/math.sqrt(3), np.array(avg_RMSE_val_ec["KNN"]) - t_025_2*np.array(avg_RMSE_val_ec_std["KNN"])/math.sqrt(3), facecolor=col_methods["knn"], alpha=0.3)
axs[1].plot(x, avg_RMSE_val_ec["RF"],  c = col_methods["rf"],label = "RF", linestyle = "--", marker = "o")
axs[1].fill_between(x, np.array(avg_RMSE_val_ec["RF"]) + t_025_2*np.array(avg_RMSE_val_ec_std["RF"])/math.sqrt(3), np.array(avg_RMSE_val_ec["RF"]) - t_025_2*np.array(avg_RMSE_val_ec_std["RF"])/math.sqrt(3), facecolor=col_methods["rf"], alpha=0.3)
axs[1].plot(x, avg_RMSE_val_ec["SVR"],  c = col_methods["svr"],label = "SVR", linestyle = "--", marker = "o")
axs[1].fill_between(x, np.array(avg_RMSE_val_ec["SVR"]) + t_025_2*np.array(avg_RMSE_val_ec_std["SVR"])/math.sqrt(3), np.array(avg_RMSE_val_ec["SVR"]) - t_025_2*np.array(avg_RMSE_val_ec_std["SVR"])/math.sqrt(3), facecolor=col_methods["svr"], alpha=0.3)

#axs[1].axhline(y = 22.65, ls = "--", c = col_methods["mlr"])
#axs[1].axhline(y = 18.39, ls = "--", c = col_methods["knn"])
#axs[1].axhline(y = 19.19,  ls = "--", c = col_methods["rf"])
#axs[1].axhline(y = 18.93,  ls = "--", c = col_methods["svr"])
## 2017
axs[1].axhline(y = 9.29, ls = "--", c = col_methods["mlr"])
axs[1].axhline(y = 8.39, ls = "--", c = col_methods["knn"])
axs[1].axhline(y = 8.4,  ls = "--", c = col_methods["rf"])
axs[1].axhline(y = 7.97,  ls = "--", c = col_methods["svr"])
#

#
#axs[1].axhline(y = 21.02,  ls = "--", c = col_methods["mlr"])
#axs[1].axhline(y = 19.27,  ls = "--", c = col_methods["knn"])
#axs[1].axhline(y = 19.58,  ls = "--", c = col_methods["rf"])
#axs[1].axhline(y = 19.65,  ls = "--", c = col_methods["svr"])

#ax.plot(0, 9.29, c = col_methods["mlr"], linestyle = "--",marker = "o")
#ax.plot(0, 8.39 ,  c = col_methods["knn"], linestyle = "--", marker = "o")
#ax.plot(0, 8.4 ,  c = col_methods["rf"], linestyle = "--", marker = "o")
#ax.plot(0, 7.97,  c = col_methods["svr"], linestyle = "--", marker = "o")
axs[0].set_title("MOX")
axs[1].set_title("EC + MOX")
axs[0].set_xlabel("# MOX", fontsize = 12)
axs[1].set_xlabel("# MOX", fontsize = 12)
axs[0].set_xlim(0.5,4.5)
axs[1].set_xlim(0.5,4.5)
axs[0].set_ylim(5, 13)
axs[0].set_ylabel("RMSE ($\mu gr/m^3$)", fontsize = 12)
#plt.legend(ncol = 2, frameon = False)
axs[0].xaxis.set_major_locator(MaxNLocator(integer=True))
axs[1].xaxis.set_major_locator(MaxNLocator(integer=True))
axs[1].legend()
axs[0].legend()
plt.savefig("test-captor-raptor-tona-2017-fusion.png", bbox_inches = "tight")
plt.show()




#%% RAPTOR 2018



folder = os.getcwd() + "/Results2/MLR_PLS/"
file_rd = "RAP-CAP-2018-2_"
combinations = {}
combinations['1'] = ['s2']
combinations['2'] = [['s1', 's2'], ['s2','s3'], ['s2','s4']]
combinations['3'] = [['s1', 's2', 's3'], ['s1', 's2', 's4'], ['s2', 's3', 's4']]
combinations['4'] = [['s1', 's2', 's3', 's4']]
combinations_keys = ['1', '2', '3', '4']


avg_RMSE_val = {}
avg_RMSE_val_std = {}
for method in ["MLR", "KNN", "RF", "SVR"]:
        avg_RMSE_val[method] = []
        avg_RMSE_val_std[method] = []
        folder = os.getcwd() + "/Results2/" + method + "_PLS/"
        for key in combinations_keys:
                aux_avg_val_RMSE = []
                if key != '1':
                        for comb in combinations[key]:
                                y_predicted_train = pd.DataFrame()
                                y_predicted_test  = pd.DataFrame()
                                y_test = []
                                for prefix in comb:
                                        file_to_rd = file_rd + prefix
                                        data = pd.read_csv(folder + file_to_rd + ".csv", sep = ";")
                                        y_predicted_train[prefix] = data['5'].dropna() - data['6'].dropna()
                                        print(np.mean(y_predicted_train[prefix]))
                                        y_predicted_test[prefix] = data['0'].dropna()
                                        y_test = data['1'].dropna()
                                        # Apply sensor fusion algorithm

                                rmse_test = sensor_fusion_combination(y_predicted_train, y_predicted_test, y_test)
                                aux_avg_val_RMSE.append(rmse_test)
                        avg_RMSE_val[method].append(np.mean(aux_avg_val_RMSE))
                else:
                        file_to_rd = file_rd + combinations[key][0]
                        data = pd.read_csv(folder + file_to_rd + ".csv", sep = ";")
                        avg_RMSE_val[method].append(data['3'][1])

                if key == '1' or key == '4':
                        avg_RMSE_val_std[method].append(0)
                else:
                        avg_RMSE_val_std[method].append(np.std(aux_avg_val_RMSE, ddof = 1))

file_rd = "RAP-CAP-2018-2_"
combinations = {}
combinations['1'] = [['s2', 's1_ec']]
combinations['2'] = [['s1', 's2', 's1_ec'], ['s2','s3', 's1_ec'], ['s2','s4', 's1_ec']]
combinations['3'] = [['s1', 's2', 's3', 's1_ec'], ['s1', 's2', 's4', 's1_ec'], ['s2', 's3', 's4', 's1_ec']]
combinations['4'] = [['s1', 's2', 's3', 's4', 's1_ec']]
combinations_keys = ['1', '2', '3', '4']
#combinations['1'] = ['s2']
#combinations['2'] = ['s2_s1', 's2_s3', 's2_s4']
#combinations['3'] = ['s2_s1_s3', 's2_s1_s4', 's2_s3_s4']
#combinations['4'] = ['s1_s2_s3_s4']

avg_RMSE_val_ec = {}
avg_RMSE_val_ec_std = {}
for method in ["MLR", "KNN", "RF", "SVR"]:
        avg_RMSE_val_ec[method] = []
        avg_RMSE_val_ec_std[method] = []
        folder = os.getcwd() + "/Results2/" + method + "_PLS/"
        for key in combinations_keys:
                aux_avg_val_RMSE = []
                if key != '6':
                        for comb in combinations[key]:
                                y_predicted_train = pd.DataFrame()
                                y_predicted_test  = pd.DataFrame()
                                y_test = []
                                for prefix in comb:
                                        file_to_rd = file_rd  + prefix
                                        data = pd.read_csv(folder + file_to_rd + ".csv", sep = ";")
                                        y_predicted_train[prefix] = data['5'].dropna() - (data['6'].dropna())
                                        y_predicted_test[prefix] = data['0'].dropna()
                                        y_test = data['1'].dropna()
                                        # Apply sensor fusion algorithm

                                rmse_test = sensor_fusion_combination(y_predicted_train, y_predicted_test, y_test)
                                aux_avg_val_RMSE.append(rmse_test)
                        avg_RMSE_val_ec[method].append(np.mean(aux_avg_val_RMSE))
                else:
                        file_to_rd = file_rd + combinations[key][0]
                        data = pd.read_csv(folder  + file_to_rd + ".csv", sep = ";")
                        avg_RMSE_val_ec[method].append(data['3'][1])

                if key == '1' or key == '4':
                        avg_RMSE_val_ec_std[method].append(0)
                else:
                        avg_RMSE_val_ec_std[method].append(np.std(aux_avg_val_RMSE, ddof = 1))


mpl.rcParams['text.latex.preamble']=[r"\usepackage{amsmath}"]
mpl.rcParams['figure.figsize'] = 6, 5.5
color = ['black','red','blue','magenta','green','orange']
col_methods = {}
col_methods["mlr"] = color[3]
col_methods["knn"] = color[1]
col_methods["rf"] = color[2]
col_methods["svr"] = color[4]
plt.rc('font', family='serif')



mpl.rcParams['figure.figsize'] = 8, 5
fig, axs = plt.subplots(1, 2, sharey=True)
# Remove horizontal space between axes
#yticks = ticker.MaxNLocator(nbins = 10)
#axs[0].xaxis.set_major_locator(yticks)
#axs[1].xaxis.set_major_locator(yticks)
x = np.arange(start=1, stop = 5, step = 1)

fig.subplots_adjust(hspace=0)
plt.setp(axs[0].get_xticklabels(), visible=True)
axs[0].plot(x, avg_RMSE_val["MLR"],  c = col_methods["mlr"], marker = "o")
axs[0].fill_between(x, np.array(avg_RMSE_val["MLR"]) + t_025_2*np.array(avg_RMSE_val_std["MLR"])/math.sqrt(3), np.array(avg_RMSE_val["MLR"]) - t_025_2*np.array(avg_RMSE_val_std["MLR"])/math.sqrt(3), facecolor=col_methods["mlr"], alpha=0.3)
axs[0].plot(x, avg_RMSE_val["KNN"],  c = col_methods["knn"], marker = "o")
axs[0].fill_between(x, np.array(avg_RMSE_val["KNN"]) + t_025_2*np.array(avg_RMSE_val_std["KNN"])/math.sqrt(3), np.array(avg_RMSE_val["KNN"]) - t_025_2*np.array(avg_RMSE_val_std["KNN"])/math.sqrt(3), facecolor=col_methods["knn"], alpha=0.3)

axs[0].plot(x, avg_RMSE_val["RF"], c = col_methods["rf"], marker = "o")
axs[0].fill_between(x, np.array(avg_RMSE_val["RF"]) + t_025_2*np.array(avg_RMSE_val_std["RF"])/math.sqrt(3), np.array(avg_RMSE_val["RF"]) - t_025_2*np.array(avg_RMSE_val_std["RF"])/math.sqrt(3), facecolor=col_methods["rf"], alpha=0.3)

axs[0].plot(x, avg_RMSE_val["SVR"], c = col_methods["svr"], marker = "o")
axs[0].fill_between(x, np.array(avg_RMSE_val["SVR"]) + t_025_2*np.array(avg_RMSE_val_std["SVR"])/math.sqrt(3), np.array(avg_RMSE_val["SVR"]) - t_025_2*np.array(avg_RMSE_val_std["SVR"])/math.sqrt(3), facecolor=col_methods["svr"], alpha=0.3)

axs[0].axhline(y = 22.65, label = "MLR EC", ls = "--", c = col_methods["mlr"])
axs[0].axhline(y = 18.39, label = "KNN EC", ls = "--", c = col_methods["knn"])
axs[0].axhline(y = 19.19, label = "RF EC", ls = "--", c = col_methods["rf"])
axs[0].axhline(y = 18.93, label = "SVR EC", ls = "--", c = col_methods["svr"])

## 2017
#axs[0].axhline(y = 9.29, label = "MLR EC", ls = "--", c = col_methods["mlr"])
#axs[0].axhline(y = 8.39, label = "KNN EC", ls = "--", c = col_methods["knn"])
#axs[0].axhline(y = 8.4, label = "RF EC", ls = "--", c = col_methods["rf"])
#axs[0].axhline(y = 7.97, label = "SVR EC", ls = "--", c = col_methods["svr"])

## 2019 FUSION
#axs[0].axhline(y = 21.02, label = "MLR EC", ls = "--", c = col_methods["mlr"])
#axs[0].axhline(y = 19.27, label = "KNN EC", ls = "--", c = col_methods["knn"])
#axs[0].axhline(y = 19.58, label = "RF EC", ls = "--", c = col_methods["rf"])
#axs[0].axhline(y = 19.65, label = "SVR EC", ls = "--", c = col_methods["svr"])
#

axs[1].plot(x, avg_RMSE_val_ec["MLR"], c = col_methods["mlr"],label = "MLR", linestyle = "--",marker = "o")
axs[1].fill_between(x, np.array(avg_RMSE_val_ec["MLR"]) + t_025_2*np.array(avg_RMSE_val_ec_std["MLR"])/math.sqrt(4), np.array(avg_RMSE_val_ec["MLR"]) - t_025_2*np.array(avg_RMSE_val_ec_std["MLR"])/math.sqrt(3), facecolor=col_methods["mlr"], alpha=0.3)
axs[1].plot(x, avg_RMSE_val_ec["KNN"],  c = col_methods["knn"],label = "KNN", linestyle = "--", marker = "o")
axs[1].fill_between(x, np.array(avg_RMSE_val_ec["KNN"]) + t_025_2*np.array(avg_RMSE_val_ec_std["KNN"])/math.sqrt(4), np.array(avg_RMSE_val_ec["KNN"]) - t_025_2*np.array(avg_RMSE_val_ec_std["KNN"])/math.sqrt(3), facecolor=col_methods["knn"], alpha=0.3)
axs[1].plot(x, avg_RMSE_val_ec["RF"],  c = col_methods["rf"],label = "RF", linestyle = "--", marker = "o")
axs[1].fill_between(x, np.array(avg_RMSE_val_ec["RF"]) + t_025_2*np.array(avg_RMSE_val_ec_std["RF"])/math.sqrt(4), np.array(avg_RMSE_val_ec["RF"]) - t_025_2*np.array(avg_RMSE_val_ec_std["RF"])/math.sqrt(3), facecolor=col_methods["rf"], alpha=0.3)
axs[1].plot(x, avg_RMSE_val_ec["SVR"],  c = col_methods["svr"],label = "SVR", linestyle = "--", marker = "o")
axs[1].fill_between(x, np.array(avg_RMSE_val_ec["SVR"]) + t_025_2*np.array(avg_RMSE_val_ec_std["SVR"])/math.sqrt(4), np.array(avg_RMSE_val_ec["SVR"]) - t_025_2*np.array(avg_RMSE_val_ec_std["SVR"])/math.sqrt(3), facecolor=col_methods["svr"], alpha=0.3)

axs[1].axhline(y = 22.65, ls = "--", c = col_methods["mlr"])
axs[1].axhline(y = 18.39, ls = "--", c = col_methods["knn"])
axs[1].axhline(y = 19.19,  ls = "--", c = col_methods["rf"])
axs[1].axhline(y = 18.93,  ls = "--", c = col_methods["svr"])
### 2017
#axs[1].axhline(y = 9.29, ls = "--", c = col_methods["mlr"])
#axs[1].axhline(y = 8.39, ls = "--", c = col_methods["knn"])
#axs[1].axhline(y = 8.4,  ls = "--", c = col_methods["rf"])
#axs[1].axhline(y = 7.97,  ls = "--", c = col_methods["svr"])
##

#
#axs[1].axhline(y = 21.02,  ls = "--", c = col_methods["mlr"])
#axs[1].axhline(y = 19.27,  ls = "--", c = col_methods["knn"])
#axs[1].axhline(y = 19.58,  ls = "--", c = col_methods["rf"])
#axs[1].axhline(y = 19.65,  ls = "--", c = col_methods["svr"])

#ax.plot(0, 9.29, c = col_methods["mlr"], linestyle = "--",marker = "o")
#ax.plot(0, 8.39 ,  c = col_methods["knn"], linestyle = "--", marker = "o")
#ax.plot(0, 8.4 ,  c = col_methods["rf"], linestyle = "--", marker = "o")
#ax.plot(0, 7.97,  c = col_methods["svr"], linestyle = "--", marker = "o")
axs[0].set_title("MOX")
axs[1].set_title("EC + MOX")
axs[0].set_xlabel("# MOX", fontsize = 12)
axs[1].set_xlabel("# MOX", fontsize = 12)
axs[0].set_xlim(0.5,4.5)
axs[1].set_xlim(0.5,4.5)
axs[0].set_ylim(17, 32.5)
axs[0].set_ylabel("RMSE ($\mu gr/m^3$)", fontsize = 12)
#plt.legend(ncol = 2, frameon = False)
axs[0].xaxis.set_major_locator(MaxNLocator(integer=True))
axs[1].xaxis.set_major_locator(MaxNLocator(integer=True))
axs[1].legend()
axs[0].legend()
plt.savefig("test-captor-raptor-tona-2018-fusion.png", bbox_inches = "tight")
plt.show()






#%% RAPTOR 2019



folder = os.getcwd() + "/Results2/MLR_PLS/"
file_rd = "RAP-CAP-2019_"
combinations = {}
combinations['1'] = ['s3']
combinations['2'] = [['s3', 's1'], ['s3', 's2'], ['s3', 's4']]
combinations['3'] = [['s3', 's1', 's2'], ['s3', 's1', 's4'], ['s3', 's2', 's4']]
combinations['4'] = [['s1', 's2', 's3', 's4']]
combinations_keys = ['1', '2', '3', '4']


avg_RMSE_val = {}
avg_RMSE_val_std = {}
for method in ["MLR", "KNN", "RF", "SVR"]:
        avg_RMSE_val[method] = []
        avg_RMSE_val_std[method] = []
        folder = os.getcwd() + "/Results2/" + method + "_PLS/"
        for key in combinations_keys:
                aux_avg_val_RMSE = []
                if key != '1':
                        for comb in combinations[key]:
                                y_predicted_train = pd.DataFrame()
                                y_predicted_test  = pd.DataFrame()
                                y_test = []
                                for prefix in comb:
                                        file_to_rd = file_rd + prefix
                                        data = pd.read_csv(folder + file_to_rd + ".csv", sep = ";")
                                        y_predicted_train[prefix] = data['5'].dropna() - data['6'].dropna()
                                        y_predicted_test[prefix] = data['0'].dropna()
                                        y_test = data['1'].dropna()
                                        # Apply sensor fusion algorithm

                                rmse_test = sensor_fusion_combination(y_predicted_train, y_predicted_test, y_test)
                                print(rmse_test)
                                aux_avg_val_RMSE.append(rmse_test)
                        avg_RMSE_val[method].append(np.mean(aux_avg_val_RMSE))
                else:
                        file_to_rd = file_rd + combinations[key][0]
                        data = pd.read_csv(folder + file_to_rd + ".csv", sep = ";")
                        avg_RMSE_val[method].append(data['3'][1])

                if key == '1' or key == '4':
                        avg_RMSE_val_std[method].append(0)
                else:
                        avg_RMSE_val_std[method].append(np.std(aux_avg_val_RMSE, ddof = 1))
#%%
file_rd = "RAP-CAP-2019_"
combinations = {}
combinations['1'] = [['s3', 's1_ec']]
combinations['2'] = [['s3', 's1', 's1_ec'], ['s3', 's2', 's1_ec'], ['s3', 's4', 's1_ec']]
combinations['3'] = [['s3', 's1', 's2', 's1_ec'], ['s3', 's1', 's4', 's1_ec'], ['s3', 's2', 's4', 's1_ec']]
combinations['4'] = [['s1', 's2', 's3', 's4', 's1_ec']]
combinations_keys = ['1', '2', '3', '4']
#combinations['1'] = ['s2']
#combinations['2'] = ['s2_s1', 's2_s3', 's2_s4']
#combinations['3'] = ['s2_s1_s3', 's2_s1_s4', 's2_s3_s4']
#combinations['4'] = ['s1_s2_s3_s4']

avg_RMSE_val_ec = {}
avg_RMSE_val_ec_std = {}
for method in ["MLR", "KNN", "RF", "SVR"]:
        avg_RMSE_val_ec[method] = []
        avg_RMSE_val_ec_std[method] = []
        folder = os.getcwd() + "/Results2/" + method + "_PLS/"
        for key in combinations_keys:
                aux_avg_val_RMSE = []
                if key != '6':
                        for comb in combinations[key]:
                                y_predicted_train = pd.DataFrame()
                                y_predicted_test  = pd.DataFrame()
                                y_test = []
                                for prefix in comb:
                                        file_to_rd = file_rd  + prefix
                                        data = pd.read_csv(folder + file_to_rd + ".csv", sep = ";")
                                        y_predicted_train[prefix] = data['5'].dropna() - (data['6'].dropna())
                                        y_predicted_test[prefix] = data['0'].dropna()
                                        y_test = data['1'].dropna()
                                        # Apply sensor fusion algorithm

                                rmse_test = sensor_fusion_combination(y_predicted_train, y_predicted_test, y_test)
                                aux_avg_val_RMSE.append(rmse_test)
                        avg_RMSE_val_ec[method].append(np.mean(aux_avg_val_RMSE))
                else:
                        file_to_rd = file_rd + combinations[key][0]
                        data = pd.read_csv(folder  + file_to_rd + ".csv", sep = ";")
                        avg_RMSE_val_ec[method].append(data['3'][1])

                if key == '1' or key == '4':
                        avg_RMSE_val_ec_std[method].append(0)
                else:
                        avg_RMSE_val_ec_std[method].append(np.std(aux_avg_val_RMSE, ddof = 1))


mpl.rcParams['text.latex.preamble']=[r"\usepackage{amsmath}"]
mpl.rcParams['figure.figsize'] = 6, 5.5
color = ['black','red','blue','magenta','green','orange']
col_methods = {}
col_methods["mlr"] = color[3]
col_methods["knn"] = color[1]
col_methods["rf"] = color[2]
col_methods["svr"] = color[4]
plt.rc('font', family='serif')



mpl.rcParams['figure.figsize'] = 8, 5
fig, axs = plt.subplots(1, 2, sharey=True)
# Remove horizontal space between axes
#yticks = ticker.MaxNLocator(nbins = 10)
#axs[0].xaxis.set_major_locator(yticks)
#axs[1].xaxis.set_major_locator(yticks)
x = np.arange(start=1, stop = 5, step = 1)
t_025_2 = 4.303
fig.subplots_adjust(hspace=0)
plt.setp(axs[0].get_xticklabels(), visible=True)
axs[0].plot(x, avg_RMSE_val["MLR"],  c = col_methods["mlr"], marker = "o")
axs[0].fill_between(x, np.array(avg_RMSE_val["MLR"]) + t_025_2*np.array(avg_RMSE_val_std["MLR"])/math.sqrt(3), np.array(avg_RMSE_val["MLR"]) - t_025_2*np.array(avg_RMSE_val_std["MLR"])/math.sqrt(3), facecolor=col_methods["mlr"], alpha=0.3)
axs[0].plot(x, avg_RMSE_val["KNN"],  c = col_methods["knn"], marker = "o")
axs[0].fill_between(x, np.array(avg_RMSE_val["KNN"]) + t_025_2*np.array(avg_RMSE_val_std["KNN"])/math.sqrt(3), np.array(avg_RMSE_val["KNN"]) - t_025_2*np.array(avg_RMSE_val_std["KNN"])/math.sqrt(3), facecolor=col_methods["knn"], alpha=0.3)

axs[0].plot(x, avg_RMSE_val["RF"], c = col_methods["rf"], marker = "o")
axs[0].fill_between(x, np.array(avg_RMSE_val["RF"]) + t_025_2*np.array(avg_RMSE_val_std["RF"])/math.sqrt(3), np.array(avg_RMSE_val["RF"]) - t_025_2*np.array(avg_RMSE_val_std["RF"])/math.sqrt(3), facecolor=col_methods["rf"], alpha=0.3)

axs[0].plot(x, avg_RMSE_val["SVR"], c = col_methods["svr"], marker = "o")
axs[0].fill_between(x, np.array(avg_RMSE_val["SVR"]) + t_025_2*np.array(avg_RMSE_val_std["SVR"])/math.sqrt(3), np.array(avg_RMSE_val["SVR"]) - t_025_2*np.array(avg_RMSE_val_std["SVR"])/math.sqrt(3), facecolor=col_methods["svr"], alpha=0.3)

#axs[0].axhline(y = 22.65, label = "MLR EC", ls = "--", c = col_methods["mlr"])
#axs[0].axhline(y = 18.39, label = "KNN EC", ls = "--", c = col_methods["knn"])
#axs[0].axhline(y = 19.19, label = "RF EC", ls = "--", c = col_methods["rf"])
#axs[0].axhline(y = 18.93, label = "SVR EC", ls = "--", c = col_methods["svr"])

## 2017
#axs[0].axhline(y = 9.29, label = "MLR EC", ls = "--", c = col_methods["mlr"])
#axs[0].axhline(y = 8.39, label = "KNN EC", ls = "--", c = col_methods["knn"])
#axs[0].axhline(y = 8.4, label = "RF EC", ls = "--", c = col_methods["rf"])
#axs[0].axhline(y = 7.97, label = "SVR EC", ls = "--", c = col_methods["svr"])

## 2019 FUSION
axs[0].axhline(y = 21.02, label = "MLR EC", ls = "--", c = col_methods["mlr"])
axs[0].axhline(y = 19.27, label = "KNN EC", ls = "--", c = col_methods["knn"])
axs[0].axhline(y = 19.58, label = "RF EC", ls = "--", c = col_methods["rf"])
axs[0].axhline(y = 19.65, label = "SVR EC", ls = "--", c = col_methods["svr"])


axs[1].plot(x, avg_RMSE_val_ec["MLR"], c = col_methods["mlr"],label = "MLR", linestyle = "--",marker = "o")
axs[1].fill_between(x, np.array(avg_RMSE_val_ec["MLR"]) + t_025_2*np.array(avg_RMSE_val_ec_std["MLR"])/math.sqrt(3), np.array(avg_RMSE_val_ec["MLR"]) - t_025_2*np.array(avg_RMSE_val_ec_std["MLR"])/math.sqrt(3), facecolor=col_methods["mlr"], alpha=0.3)
axs[1].plot(x, avg_RMSE_val_ec["KNN"],  c = col_methods["knn"],label = "KNN", linestyle = "--", marker = "o")
axs[1].fill_between(x, np.array(avg_RMSE_val_ec["KNN"]) + t_025_2*np.array(avg_RMSE_val_ec_std["KNN"])/math.sqrt(3), np.array(avg_RMSE_val_ec["KNN"]) - t_025_2*np.array(avg_RMSE_val_ec_std["KNN"])/math.sqrt(3), facecolor=col_methods["knn"], alpha=0.3)
axs[1].plot(x, avg_RMSE_val_ec["RF"],  c = col_methods["rf"],label = "RF", linestyle = "--", marker = "o")
axs[1].fill_between(x, np.array(avg_RMSE_val_ec["RF"]) + t_025_2*np.array(avg_RMSE_val_ec_std["RF"])/math.sqrt(3), np.array(avg_RMSE_val_ec["RF"]) - t_025_2*np.array(avg_RMSE_val_ec_std["RF"])/math.sqrt(3), facecolor=col_methods["rf"], alpha=0.3)
axs[1].plot(x, avg_RMSE_val_ec["SVR"],  c = col_methods["svr"],label = "SVR", linestyle = "--", marker = "o")
axs[1].fill_between(x, np.array(avg_RMSE_val_ec["SVR"]) + t_025_2*np.array(avg_RMSE_val_ec_std["SVR"])/math.sqrt(3), np.array(avg_RMSE_val_ec["SVR"]) - t_025_2*np.array(avg_RMSE_val_ec_std["SVR"])/math.sqrt(3), facecolor=col_methods["svr"], alpha=0.3)

#axs[1].axhline(y = 22.65, ls = "--", c = col_methods["mlr"])
#axs[1].axhline(y = 18.39, ls = "--", c = col_methods["knn"])
#axs[1].axhline(y = 19.19,  ls = "--", c = col_methods["rf"])
#axs[1].axhline(y = 18.93,  ls = "--", c = col_methods["svr"])
### 2017
#axs[1].axhline(y = 9.29, ls = "--", c = col_methods["mlr"])
#axs[1].axhline(y = 8.39, ls = "--", c = col_methods["knn"])
#axs[1].axhline(y = 8.4,  ls = "--", c = col_methods["rf"])
#axs[1].axhline(y = 7.97,  ls = "--", c = col_methods["svr"])
##

#
axs[1].axhline(y = 21.02,  ls = "--", c = col_methods["mlr"])
axs[1].axhline(y = 19.27,  ls = "--", c = col_methods["knn"])
axs[1].axhline(y = 19.58,  ls = "--", c = col_methods["rf"])
axs[1].axhline(y = 19.65,  ls = "--", c = col_methods["svr"])


axs[0].set_title("MOX")
axs[1].set_title("EC + MOX")
axs[0].set_xlabel("# MOX", fontsize = 12)
axs[1].set_xlabel("# MOX", fontsize = 12)
axs[0].set_xlim(0.5,4.5)
axs[1].set_xlim(0.5,4.5)
axs[0].set_ylim(15, 21.5)
axs[0].set_ylabel("RMSE ($\mu gr/m^3$)", fontsize = 12)
#plt.legend(ncol = 2, frameon = False)
axs[0].xaxis.set_major_locator(MaxNLocator(integer=True))
axs[1].xaxis.set_major_locator(MaxNLocator(integer=True))
axs[1].legend()
axs[0].legend()
plt.savefig("test-captor-raptor-tona-2019-fusion.png", bbox_inches = "tight")
plt.show()
